const FETCH_TODOS_BEGIN = 'FETCH_TODOS_BEGIN';
const FETCH_TODOS_SUCCESS = 'FETCH_TODOS_SUCCESS';
const FETCH_TODOS_ERROR = 'FETCH_TODOS_ERROR';
const SET_DATASEARCH = 'SET_DATASEARCH';
const SET_ID = 'SET_ID';


export const fetchUser = (userId) => {
  let url = '';
  if (userId) {
    url = ' https://jsonplaceholder.typicode.com/users?id=' + userId;

  } else {
    url = 'https://jsonplaceholder.typicode.com/users';
  }
  return dispatch => {
    dispatch(fetchTodoBegin())
    fetch(url)
      .then(response => response.json())
      .then(data => {
        dispatch(fetchTodoSuccess(data))
      })
      .catch(error => dispatch(fetchTodoError(error)))
  }
}
export const fetchTodoBegin = () => {
  return {
    type: FETCH_TODOS_BEGIN
  }
}

export const fetchTodoSuccess = todos => {
  return {
    type: FETCH_TODOS_SUCCESS,
    payLoad: todos
  }
}
export const fetchTodoError = error => {
  return {
    type: FETCH_TODOS_ERROR,
    payLoad: error
  }
}
export const setDataSearch = (text) => {
  return {
    type: SET_DATASEARCH,
    payLoad: text
  }
}
export const setId = (id) => {
  return {
    type: SET_ID,
    id
  }
}