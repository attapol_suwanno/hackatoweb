const FETCH_TODOS_BEGIN = 'FETCH_TODOS_BEGIN';
const FETCH_TODOS_ERROR = 'FETCH_TODOS_ERROR';
const SET_DATASEARCH = 'SET_DATASEARCH';
const SET_DATALIST = 'SET_DATALIST';
const SET_CLICKDONE = 'SET_CLICKDONE';

export const fetchTODoList = (userId) => {
  let urllist = '';
  if (userId) {
    urllist = 'https://jsonplaceholder.typicode.com/todos?userId=' + userId;
  } else {
    urllist = 'https://jsonplaceholder.typicode.com/todos';

  }
  
  return dispatch => {
    dispatch(fetchTodoListBegin())
    fetch(urllist)
      .then(response => response.json())
      .then(data => {
        dispatch(setDataList(data))
      })
      .catch(error => dispatch(fetchTodoListError(error)))
  }
}

export const fetchTodoListBegin = () => {
  return {
    type: FETCH_TODOS_BEGIN
  }
}

export const fetchTodoListError = error => {
  return {
    type: FETCH_TODOS_ERROR,
    payLoad: error

  }
}

export const setDataListSearch = (data) => {
  return {
    type: SET_DATASEARCH,
    payLoad: data

  }
}

export const setDataList = (list) => {
  return {
    type: SET_DATALIST,
    payLoad: list

  }
}

export const setclickDone = (id) => {
  return {
    type: SET_CLICKDONE,
    payLoad: id

  }
}
