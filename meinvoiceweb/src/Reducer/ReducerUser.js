const FETCH_TODOS_BEGIN = 'FETCH_TODOS_BEGIN';
const FETCH_TODOS_SUCCESS = 'FETCH_TODOS_SUCCESS';
const FETCH_TODOS_ERROR = 'FETCH_TODOS_ERROR';
const SET_DATASEARCH = 'SET_DATASEARCH';
const SET_ID = 'SET_ID';

const initialState = {
  todos: [],
  loading: false,
  error: '',
  searchtext: '',
  id: ''
}
export const userReducer = (state = initialState, action) => {
  switch (action.type) {

    case FETCH_TODOS_BEGIN:
      return {
        ...state,
        loading: true
      }

    case FETCH_TODOS_SUCCESS:
      return {
        ...state,
        loading: false,
        error: '',
        todos: action.payLoad
      }
    case FETCH_TODOS_ERROR:
      return {
        ...state,
        loading: false,
        error: action.payLoad
      }
    case SET_DATASEARCH:
      return {
        ...state,
        searchtext: action.payLoad

      }
    case SET_ID:
      return {
        ...state,
        setid: action.id

      }
    default:
      return state
  }
}



