import React, { useState } from 'react';
import { Layout, Row, Col, Typography, Button, Input, Modal, Table, Tag, Upload, message, DatePicker, Space } from 'antd';
import moment from 'moment';
import { UploadOutlined } from '@ant-design/icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { library } from '@fortawesome/fontawesome-svg-core'
import { fas } from "@fortawesome/free-solid-svg-icons";
import { fab } from "@fortawesome/free-brands-svg-icons";
import { faTable } from "@fortawesome/free-solid-svg-icons";
const { Column, ColumnGroup } = Table;

const Create = () => {
    const { Title, Paragraph, Text } = Typography;
    const { TextArea } = Input;
    const { Header, Footer, Content, } = Layout;
    const [loading, setLoading] = useState(false)
    const [visible, setVisible] = useState(false)
    const [loadingEdit, setLoadingEdit] = useState(false)
    const [visibleEdit, setVisibleEdit] = useState(false)
    const [startDate, setStartDate] = useState(new Date());
    const dateFormat = 'YYYY/MM/DD';



    const data = [
        {
            key: '1',
            name: 'John Brown',
            age: 32,
            address: 'New York No. 1 Lake Park',
            tags: ['nice', 'developer'],
        },
        {
            key: '2',
            name: 'Jim Green',
            age: 42,
            address: 'London No. 1 Lake Park',
            tags: ['loser'],
        },
    ];

    const props = {
        name: 'file',
        action: 'https://www.mocky.io/v2/5cc8019d300000980a055e76',
        headers: {
            authorization: 'authorization-text',
        },
        onChange(info) {
            if (info.file.status !== 'uploading') {
                console.log(info.file, info.fileList);
            }
            if (info.file.status === 'done') {
                message.success(`${info.file.name} file uploaded successfully`);
            } else if (info.file.status === 'error') {
                message.error(`${info.file.name} file upload failed.`);
            }
        },
    };

    const showModal = () => {
        setVisible(true)
    };

    const handleOk = () => {
        setVisible(true);
        setTimeout(() => {
            setVisible(true);
            setLoading(false);
        }, 3000);
    };

    const handleCancel = () => {
        setVisible(false);
    };

    const showModalEdit = () => {
        setVisibleEdit(true)
    };

    const handleOkEdit = () => {
        setVisibleEdit(true);
        setTimeout(() => {
            setVisibleEdit(true);
            setLoadingEdit(false);
        }, 3000);
    };

    const handleCancelEdit = () => {
        setVisibleEdit(false);
    };

    return (
        <div >
            <Layout >
                <Row >
                    <Col span={12} onClick={() => { window.location = "/" }} >
                        <Col span={3} style={{ marginLeft: '20%' }}>
                            <div style={{ fontSize: 54, color: '#747272' }} > Me </div>
                        </Col>
                        <Col span={12} >
                            <div style={{ fontSize: 54, color: '#93BE9D' }}> Invoice </div>
                        </Col>
                    </Col>
                    <Col span={12}>
                        <Button style={{ backgroundColor: '#93BE9D', marginLeft: '40%', marginTop: '5%', fontSize: 24, height: 40 }} value="Logout" onClick={() => { window.location = "/ViewInvoice" }} >LIST</Button>
                        <Button style={{ backgroundColor: '#93BE9D', marginLeft: '5%', fontSize: 24, height: 40 }} value="Logout" onClick={() => { window.location = "/Create" }} >CREATE</Button>
                    </Col>
                </Row>
                <Content style={{ justifyContent: 'center', margin: '10%' }}>
                    <Col span={12} >
                        <FontAwesomeIcon icon={['fas', 'building']}  className={'fa-lg'} color={'#ADADAD'} style={{marginLeft:'3%'}}  /><Text style={{ fontSize: 18, color: '#ADADAD',marginLeft:'2%' }}>Company Information</Text>
                    </Col>
                    <Col span={12} >
                        <FontAwesomeIcon icon={['fas', 'user-tie']} className={'fa-lg'} color={'#ADADAD'} style={{marginLeft:'10%' }}  /><Text style={{ fontSize: 18, color: '#ADADAD',marginLeft:'2%'  }}>Your Information</Text>
                    </Col>
                    <Row style={{marginTop:'4%'}}>
                        <Col span={12} >
                            <div style={{ backgroundColor: 'white', padding: '8%', width: '95%', height: 430, borderRadius: 8 }}>
                                <Text >Name</Text>
                                <Input placeholder="Basic usage" style={{ borderRadius: 8, marginTop: '2%' }} />
                                <Text>Text Id</Text>
                                <Input placeholder="Basic usage" style={{ borderRadius: 8, marginTop: '2%' }} />
                                <Text>E-Mail Address</Text>
                                <Input placeholder="Basic usage" style={{ borderRadius: 8, marginTop: '2%' }} />
                                <Text>Logo</Text>
                                <div>
                                    <Row>
                                        <Col>
                                            <Input placeholder="Basic usage" style={{ borderRadius: 8, marginTop: '2%', width: '80%' }} />
                                            <Upload {...props}>
                                                <Button type="primary" style={{ borderRadius: 8, height: 32, width: 80, marginLeft: '15%', backgroundColor: 'white', color: '#D9D9D9', borderColor: '#CECECE' }} >
                                                    <UploadOutlined /> Browse
                                            </Button>
                                            </Upload>
                                        </Col>
                                    </Row>
                                </div>
                                <Text style={{ color: '#ADADAD' }}>Address</Text>
                                <TextArea rows={3} />
                            </div>
                        </Col>
                        <Col span={12} >
                            <div style={{ backgroundColor: 'white', padding: '8%', width: '95%', height: 430, borderRadius: 8, marginLeft: '5%' }}>
                                <Text>Name</Text>
                                <Input placeholder="Basic usage" style={{ borderRadius: 8, marginTop: '2%' }} />
                                <Text>Text Id</Text>
                                <Input placeholder="Basic usage" style={{ borderRadius: 8, marginTop: '2%' }} />
                                <Text>E-Mail Address</Text>
                                <Input placeholder="Basic usage" style={{ borderRadius: 8, marginTop: '2%' }} />
                                <Text>Address</Text>
                                <TextArea rows={5} />
                            </div>
                        </Col>
                    </Row>
                    <Row type="flex" align="middle" style={{ marginTop: '5%' }} >
                        <FontAwesomeIcon icon={['fas', 'file']} className={'fa-lg'}  color={'#ADADAD'} style={{marginLeft:'2%'}}   /><Text style={{ fontSize: 18, color: '#CECECE',marginLeft:'2%'  }}>Invoice Information</Text>
                        <div style={{ backgroundColor: 'white', width: '100%', height: '100%', borderRadius: 8, marginTop: 10 }}>
                            <Col span={12} style={{ padding: '5%' }}>
                                <Text>No.</Text>
                                <Input placeholder="Basic usage" style={{ borderRadius: 8, marginTop: '2%' }} />
                                <Text>Issue Date</Text>
                                <DatePicker defaultValue={moment('2020/01/01', dateFormat)} format={dateFormat} style={{ borderRadius: 12, marginTop: '2%', width: '100%' }} />
                                <Text>Due Date</Text>
                                <DatePicker defaultValue={moment('2020/01/01', dateFormat)} format={dateFormat} style={{ borderRadius: 12, marginTop: '2%', width: '100%' }} />
                            </Col>
                            <Col span={12} style={{ padding: '5%', marginTop: '1%' }}>
                                <Text>Note</Text>
                                <TextArea rows={7} />
                            </Col>
                        </div>
                    </Row>
                    <Row style={{ marginTop: '5%' }}>
                        <FontAwesomeIcon icon={faTable} className={'fa-lg'} color={'#ADADAD'} style={{marginLeft:'2%'}}  /><Text style={{ fontSize: 18, color: '#CECECE',marginLeft:'2%'  }}>Items</Text>
                        <div style={{ backgroundColor: 'white', padding: '8%', width: '100%', height: '100%', borderRadius: 8, marginTop: 10 }}>
                            <Button type="primary" onClick={showModal} style={{ borderRadius: 8, height: 40, width: 160, marginLeft: '80%', backgroundColor: 'black', borderColor: 'black', marginBottom: '2%' }}>Add New Item</Button>
                            <Modal
                                visible={visible}
                                title="ADD NEW ITEM"
                                onOk={handleOk}
                                onCancel={handleCancel}
                                footer={[
                                    <div style={{ marginRight: 300 }}>
                                        <Button key="submit" type="primary" loading={loading} onClick={handleOk} style={{ borderRadius: 8, height: 40, width: 200, marginLeft: '80%', backgroundColor: '#93BE9D', borderColor: '#93BE9D' }}>
                                            ADD
                                    </Button>
                                    </div>
                                ]}
                            >
                                <Text>Desciption</Text>
                                <TextArea rows={5} />
                                <Text>Qty</Text>
                                <Input placeholder="Basic usage" style={{ borderRadius: 8, marginTop: '2%' }} />
                                <Text>Unit Price</Text>
                                <Input placeholder="Basic usage" style={{ borderRadius: 8, marginTop: '2%' }} />
                                <Text>Total Price</Text>
                                <div>
                                    <Text>{0}</Text>
                                </div>
                            </Modal>
                            <Table dataSource={data} >
                                <Column title="No." dataIndex="key" key="key" />
                                <Column title="Desciption" dataIndex="address" key="address" />
                                <Column title="Qty" dataIndex="tags" key="tags" />
                                <Column title="Unit Price" dataIndex="lastName" key="lastName" />
                                <Column title="Total Price" dataIndex="firstName" key="firstName" />
                                <Column
                                    key="action"
                                    render={(text, record) => (
                                        <div >
                                            <a> <FontAwesomeIcon icon={["fas", "pen-square"]}  color={'#ADADAD'} onClick={showModalEdit} /></a>
                                        </div>
                                    )}
                                />
                                <Column
                                    key="action"
                                    render={(text, record) => (
                                        <div >
                                            <a><FontAwesomeIcon icon={["fas", "trash"]}  color={'#ADADAD'} /></a>
                                        </div>
                                    )}
                                />
                            </Table>
                            <Modal
                                visible={visibleEdit}
                                title="EDIT ITEM"
                                onOk={handleOkEdit}
                                onCancel={handleCancelEdit}
                                footer={[
                                    <div style={{ marginRight: 300 }}>
                                        <Button key="submit" type="primary" loading={loading} onClick={handleOk} style={{ borderRadius: 8, height: 40, width: 200, marginLeft: '80%', backgroundColor: '#93BE9D', borderColor: '#93BE9D' }}>
                                            ADD
                                    </Button>
                                    </div>
                                ]}
                            >
                                <Text>Desciption</Text>
                                <TextArea rows={5} />
                                <Text>Qty</Text>
                                <Input placeholder="Basic usage" style={{ borderRadius: 8, marginTop: '2%' }} />
                                <Text>Unit Price</Text>
                                <Input placeholder="Basic usage" style={{ borderRadius: 8, marginTop: '2%' }} />
                                <Text>Total Price</Text>
                                <div>
                                    <Text style={{ backgroundColor: '#93BE9D' }}>{0}</Text>
                                </div>
                            </Modal>
                        </div>
                    </Row>
                    <Row type="flex" align="middle" style={{ marginTop: '10%' }} >
                        <Col>
                            <div style={{ display: 'inline-flex', justifyContent: 'center', alignItems: 'center', marginLeft: '70%' }}>
                                <Button type="primary" style={{ borderRadius: 8, height: 40, width: 500, backgroundColor: '#93BE9D', borderColor: '#93BE9D' }}>Submit</Button>
                            </div>
                        </Col>
                    </Row>
                </Content>
            </Layout>
        </div>
    )
}
export default (Create);