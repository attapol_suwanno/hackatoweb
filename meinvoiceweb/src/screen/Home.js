import React from 'react';
import { Layout, Row, Col, Typography, Button, List, Avatar, } from 'antd';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { library } from '@fortawesome/fontawesome-svg-core'
import { fas } from "@fortawesome/free-solid-svg-icons";
import { fab } from "@fortawesome/free-brands-svg-icons";



library.add(fas)


const { Footer, Content, } = Layout;
const Home = () => {
    const { Title, Text } = Typography;

    const listData = [];
    for (let i = 0; i < 23; i++) {
        listData.push({
            href: 'https://ant.design',
            title: `ant design part ${i}`,
            avatar: 'https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png',
            description:
                'Ant Design, a design language for background .',
            content:
                'We supply a series of design principles, practical patterns and high .',
        });
    }

    return (
        <div>
            <div>
                <Layout>
                    <Row >
                        <Col span={12} >
                            <Col span={3} style={{ marginLeft: '20%' }}>
                                <div style={{ fontSize: 54, color: '#747272' }} > Me </div>
                            </Col>
                            <Col span={12} >
                                <div style={{ fontSize: 54, color: '#93BE9D' }}> Invoice </div>
                            </Col>
                        </Col>
                        <Col span={12}>
                            <Button style={{ backgroundColor: '#93BE9D', marginLeft: '40%', marginTop: '5%', fontSize: 24, height: 40 }} value="Logout" onClick={() => { window.location = "/ViewInvoice" }} >LIST</Button>
                            <Button style={{ backgroundColor: '#93BE9D', marginLeft: '5%', fontSize: 24, height: 40 }} value="Logout" onClick={() => { window.location = "/Create" }} >CREATE</Button>
                        </Col>
                    </Row>
                    <Content style={{ justifyContent: 'center' }}>
                        <Row >
                            <List
                                style={{ width: '70%', marginLeft: '15%', marginTop: '8%' }}
                                itemLayout="vertical"
                                size="large"
                                pagination={{
                                    onChange: page => {
                                        console.log(page);
                                    },
                                    pageSize: 9,
                                }}
                                dataSource={listData}
                                footer={
                                    <div>
                                        <b>ant design</b> footer part
                                    </div>
                                }
                                renderItem={item => (
                                    <List.Item
                                        style={{ width: '100%', backgroundColor: 'white', height: 100, marginTop: '2%', borderRadius: 8 }}
                                        key={item.title}
                                    >
                                        <Row>
                                            <Col span={4} onClick={() => { window.location = "/ViewInvoice" }} style={{ padding: 16, backgroundColor: '#E5E5E5' }}  >
                                                <Row >
                                                    <Text style={{ color: '#747272', fontSize: 14 }}>No.</Text>
                                                </Row>
                                                <Row >
                                                    <Text style={{ fontSize: 16 }}>ID-2-27</Text>
                                                </Row>
                                            </Col>
                                            <Col span={12}>
                                                <List.Item.Meta
                                                    title={<a href={item.href}> <FontAwesomeIcon icon={['fas', 'building']}/>{item.title}</a>}
                                                    description={<a ><FontAwesomeIcon icon={['fas', 'user-tie']} color={ 'black' }/>{item.description}</a>}
                                                />
                                            </Col>
                                            <Col span={8} >
                                                <Row style={{ backgroundColor: '#EFFBF0', borderRadius: 20, height: 25, width: 250, textAlign: 'center', marginTop: '2%' }}>
                                                    <Text level={4} style={{ fontSize: 18 }}>Issue Date:{'14 April 2020'}</Text>
                                                </Row>
                                                <Row style={{ backgroundColor: '#93BE9D', borderRadius: 20, height: 25, width: 250, textAlign: 'center', marginTop: '1%' }} >
                                                    <Text level={4} style={{ fontSize: 18 }}>Due Date:{'14 April 2020'}</Text>
                                                </Row>
                                            </Col>
                                        </Row>
                                    </List.Item>
                                )}
                            />,
                        </Row>
                    </Content>
                    <Footer>
                    </Footer>
                </Layout>
            </div>
        </div>
    )
}
export default Home;