import React from 'react';
import { Layout, Row, Col, Typography, Button, Divider } from 'antd';
import Logo from '../Src/Logo.png'
const { Content, } = Layout;
const ViewInvoice = () => {
    const { Title, Paragraph } = Typography;


    const listData = [];
    for (let i = 0; i < 23; i++) {
        listData.push({
            href: 'https://ant.design',
            title: `ant design part ${i}`,
            avatar: 'https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png',
            description:
                'Ant Design, a design language for background .',
            content:
                'We supply a series of design principles, practical patterns and high .',
        });
    }

    return (
        <div >
            <Layout >
                <Row >
                    <Col span={12} onClick={() => { window.location = "/" }} >
                        <Col span={3} style={{ marginLeft: '20%' }}>
                            <div style={{ fontSize: 54, color: '#747272' }} > Me </div>
                        </Col>
                        <Col span={12} >
                            <div style={{ fontSize: 54, color: '#93BE9D' }}> Invoice </div>
                        </Col>
                    </Col>
                    <Col span={12}>
                        <Button style={{ backgroundColor: '#93BE9D', marginLeft: '40%', marginTop: '5%', fontSize: 24, height: 40 }} value="Logout" onClick={() => { window.location = "/ViewInvoice" }} >LIST</Button>
                        <Button style={{ backgroundColor: '#93BE9D', marginLeft: '5%', fontSize: 24, height: 40 }} value="Logout" onClick={() => { window.location = "/Create" }} >CREATE</Button>
                    </Col>
                </Row>
                <Content style={{ justifyContent: 'center', backgroundColor: 'white', margin: '10%' }}>
                    <div style={{ margin: '10%' }}>
                        <Row >
                            <Col span={12}>
                                <img
                                    width="180px"
                                    height="120px"
                                    src={Logo} />
                                <Title style={{ fontSize: 20, fontWeight: 'bold', marginTop: '2%' }}>Dragon Enterprise</Title>
                                <div>Ap #651-7643 Sodales Av.  Tamuning PA 10855</div>
                                <Title style={{ fontSize: 20, fontWeight: 'bold', marginTop: '5%' }}>TAX ID {"0105535137307"}</Title>
                                <div>contact@hotmail.com</div>
                                <div>096-724-2744</div>

                                <Title style={{ fontSize: 24, marginTop: "5%" }}>BILL TO</Title>
                                <Title style={{ fontSize: 20, fontWeight: 'bold' }}>My Customer Company</Title>
                                <div>Ap #651-7643 Sodales Av.  Tamuning PA 10855</div>


                                <Title style={{ fontSize: 20, fontWeight: 'bold', marginTop: '5%' }}>TAX ID {"03405535137307"}</Title>
                                <div>contact@hotmail.com</div>
                                <div>096-724-2744</div>
                            </Col>
                            <Col span={12} >
                                <Title level={1} style={{ fontSize: 54, fontWeight: 'bold', marginLeft: '30%' }}>Invoice</Title>
                                <div style={{ fontSize: 18, fontWeight: 'bold', marginLeft: '30%' }}>No.          {'ID-2-27'}</div>
                                <div style={{ fontSize: 18, fontWeight: 'bold', marginLeft: '30%' }}>Issue Date          {'24 March 2020'}</div>
                                <div style={{ fontSize: 18, fontWeight: 'bold', marginLeft: '30%' }}>Due Date          {'24 March 2020'}</div>
                            </Col>
                        </Row>
                        <Row style={{ backgroundColor: '#EFFBF0', padding: 10, borderRadius: 10, marginTop: '4%' }}>
                            <Col span={8}>
                                <div style={{ fontSize: 15, fontWeight: 'bold' }}>No.</div>
                            </Col>
                            <Col span={6}>
                                <div style={{ fontSize: 15, fontWeight: 'bold' }}>Description</div>
                            </Col>
                            <Col span={3}>
                                <div style={{ fontSize: 15, fontWeight: 'bold' }}>Qty</div>
                            </Col>
                            <Col span={3}>
                                <div style={{ fontSize: 15, fontWeight: 'bold' }}>Unit Price</div>
                            </Col>
                            <Col span={3}>
                                <div style={{ fontSize: 15, fontWeight: 'bold' }}>Total Price</div>
                            </Col>
                        </Row>
                        <Row style={{ backgroundColor: 'white', padding: 10, }}>
                            <Col span={8}>
                                <div>1</div>
                            </Col>
                            <Col span={6}>
                                <div>Coca Cola (Bottle)</div>
                            </Col>
                            <Col span={3}>
                                <div>20</div>
                            </Col>
                            <Col span={3}>
                                <div>14</div>
                            </Col>
                            <Col span={3}>
                                <div>240</div>
                            </Col>
                        </Row>
                        <Row style={{ marginTop: '10%' }}>
                            <Col span={12}>
                                <Title>Note</Title>
                                <Paragraph>
                                    is a long established fact that a reader will be distracted by the readable content of a page when looking at
                                    its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as
                                    opposed to using 'Content here, content here', making it look like readable English. Many desktop publishing
                            </Paragraph>
                            </Col>
                            <Col span={12} style={{ marginTop: '5%' }} >
                                <div style={{ marginLeft: '50%' }}>Sub Total {'280'}</div>
                                <div style={{ marginLeft: '50%' }}>Vat (7%) {'19.6'}</div>
                                < Divider style={{ marginLeft: '30%', borderColor: 'red' }} />
                                <div style={{ marginLeft: '50%' }}>Grand Total {'299.8'}</div>
                            </Col>

                        </Row>
                    </div>
                </Content>
            </Layout>
        </div>
    )
}
export default (ViewInvoice);