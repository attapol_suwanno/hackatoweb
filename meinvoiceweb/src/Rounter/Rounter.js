import React from "react";
import "../App.css";
import { BrowserRouter, Route, Redirect } from "react-router-dom";



import Home from '../screen/Home'
import ViewInvoice from '../screen/ViewInvoice'
import Create from '../screen/Create'

localStorage.setItem('islogin', false)
// const PrivateRoute = ({ path, component: Component }) => (
//   <Route path={path} render={(props) => {
//     const islogin = localStorage.getItem('islogin')
//     if (islogin == 'true')
//       return <Component {...props} />
//     else
//       return <Redirect to="/" />
//   }} />
// )

const Rounter = () => {
  return (
    <div>
      <BrowserRouter>
        {/* <Route path="/processlogin" render={() => {
          localStorage.setItem("islogin", true);
          return <Redirect to="/users" exact={true} />
        }} ></Route> */}
        {/* <Route path="/processlogout" render={() => {
          localStorage.setItem("islogin", false);
          return <Redirect to="/login" />
        }} ></Route> */}
        <Route path="/" component={Home} exact={true} />
        <Route path="/ViewInvoice" component={ViewInvoice} exact={true} />
        <Route path="/Create" component={Create} exact={true} />
      </BrowserRouter>
    </div>
  )
}

export default Rounter;
